## This file is written to use the function of u-net method to detect slow wave events
## Inputs: 
##        data\membrane folder:
##          test: used to tst the learning outcomes
##          train:  image: gray scale images; label: black and white images (outputs)
from model import *
from data import *
os.environ["CUDA_VISIBLE_DEVICES"] = "0"


# arguments = dict(rotation_range=0.2,
#                      width_shift_range=0.05,
#                      height_shift_range=0.05,
#                      shear_range=0.05,
#                     zoom_range=0.05,
#                     horizontal_flip=True,
#                     fill_mode='nearest')
# myGene = trainGenerator(2,'data/membrane/train','image','label',arguments,save_to_dir = None)

imgs_train,imgs_mask_train = geneTrainNpy("data/membrane/train/image/","data/membrane/train/label/")
model = unet()
model_checkpoint = ModelCheckpoint('batch2epoch10.hdf5', monitor='loss',verbose=1, save_best_only=True)
model.fit(imgs_train, imgs_mask_train, batch_size=2, nb_epoch=10, verbose=1,validation_split=0.2, shuffle=True, callbacks=[model_checkpoint])

testGene = testGenerator("data/membrane/test")
results = model.predict_generator(testGene,10,verbose=1)
saveResult("data/membrane/predict",results)
