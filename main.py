from model import *
from data import *
import matplotlib.pyplot as plt
import tensorflow as tf
import keras
from keras import layers
from keras import models
from keras import utils
from keras.layers import Dense
from keras.models import Sequential
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import Activation
from keras.regularizers import l2
from keras.optimizers import SGD
from keras.optimizers import RMSprop
from keras import datasets

from keras.callbacks import LearningRateScheduler
from keras.callbacks import History

from keras import losses


os.environ["CUDA_VISIBLE_DEVICES"] = "0"
batch_size = 2

data_gen_args = dict(rotation_range=0,
                    width_shift_range=0,
                    height_shift_range=0,
                    shear_range=0,
                    zoom_range=0,
                    horizontal_flip=True,
                    fill_mode='nearest')
myGene = trainGenerator(batch_size,'data/membrane/train','image','label',data_gen_args,save_to_dir = None)


                    
# train and save model
model = unet()

epochs = 5
steps_per_epoch = 5

# model_history = model.fit(x_train, y_train,
#                     batch_size=batch_size,
#                     epochs=epochs,
#                     verbose=1,
#                     validation_data=(x_test, y_test))
# train model (comment if already have model)
model_checkpoint = ModelCheckpoint('testtest.hdf5', monitor='loss',verbose=1, save_best_only=True)
model_history = model.fit_generator(myGene,steps_per_epoch=steps_per_epoch,epochs=epochs,callbacks=[model_checkpoint])


# Plot the loss function
# fig, plt = plt.subplots(1, 1, figsize=(10,6))
# plt.plot((model_history.history['loss']), 'r', label='train')
# #plt.plot(np.sqrt(model_history.history['val_loss']), 'b' ,label='val')
# plt.xlabel('Epoch')
# plt.ylabel('Loss')
# plt.legend()
# plt.tick_params(labelsize=20)

# # Plot the accuracy
#fig, plt = plt.subplots(1, 1, figsize=(10,6))
# plt.plot((model_history.history['acc']), 'r', label='train')
# plt.plot((model_history.history['val_acc']), 'b' ,label='val')
# plt.xlabel('Epoch')
# plt.ylabel('Accuracy')
# plt.legend()
# # plt.tick_params(labelsize=20)
# plt.show()

# test results
# seed = 1
# testGene = testGenerator("data/membrane/Gradient/test")
testGene = testGenerator("data/membrane/test")
results = model.predict_generator(testGene,10,verbose=1)
model.load_weights("Method2step50epoch5.hdf5")
saveResult("data/membrane/predict",results)
